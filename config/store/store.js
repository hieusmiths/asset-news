/* eslint-disable */
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import { appReducer } from '../../store';

// CREATING INITIAL STORE
export default initialState => {
  const store = createStore(
    appReducer,
    initialState,
    composeWithDevTools(applyMiddleware(thunkMiddleware))
  )

  // IF REDUCERS WERE CHANGED, RELOAD WITH INITIAL STATE
  if (module.hot) {
    module.hot.accept('../../store', () => {
      const createNextReducer = require('../../store').default

      store.replaceReducer(createNextReducer(initialState))
    })
  }
  return store
}