import Cookies from 'js-cookie';

export const getCookies = (string) => {
	return Cookies.get(string);
};
