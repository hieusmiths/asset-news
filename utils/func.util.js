const DATA_TYPES = {
	FUNCTION: 'function',
};

export const isNotNull = (input) => input !== null || input !== undefined;

export const isString = (input) => {
	if (typeof input === 'string') {
		return true;
	}
	return false;
};

export const isFunc = (input) => {
	const isFunction = typeof input === DATA_TYPES.FUNCTION;
	return isFunction;
};
