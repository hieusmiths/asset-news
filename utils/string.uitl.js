import parse from 'html-react-parser';

export const parseStringToHtml = (htmlString) => {
	try {
		const result = parse(htmlString);
		return result;
	} catch (error) {
		/* eslint-disable-next-line */
		console.log(error, 'CAN_NOT_PARSER_STRING_TO_HTML');
		return null;
	}
};
