/* eslint-disable import/prefer-default-export */
import ReactGA from 'react-ga';

export const initGA = () => {
	ReactGA.initialize('UA-xxxxxxxxx-1');
};
