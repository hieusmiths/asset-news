import moment from 'dayjs';
import advancedFormat from 'dayjs/plugin/advancedFormat';
import 'dayjs/locale/vi';

moment.locale('vi');
moment.extend(advancedFormat);

// export const getFromNow = ({ time }) => {
// 	try {

// 	} catch (error) {
// 		/* eslint-disable-next-line */
// 		console.log(error);
// 	}
// };

export const formatToDayMonth = ({ time }) => {
	try {
		const result = moment(time).format('DD/MM/YYYY');
		return result;
	} catch (error) {
		/* eslint-disable-next-line */
		console.log(error);
	}
};
