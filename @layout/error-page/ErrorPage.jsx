import React from 'react';

const ErrorPage = ({ statusCode }) => <h1>PAGE NOT FOUND {statusCode}</h1>;

export default ErrorPage;
