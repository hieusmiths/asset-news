import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { NavTop } from '@component';

const BaseLayout = ({ children }) => {
	return (
		<div>
			<NavTop />
			<Container className="pd-nav">
				<div>{children}</div>
			</Container>
		</div>
	);
};

export default BaseLayout;
