import BaseLayout from './base-layout';
import ErrorPage from './error-page';

export { BaseLayout, ErrorPage };
