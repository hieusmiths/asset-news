import React from 'react';
import { LOGO_PATH } from '@const';
import { NavUser } from '@component';
import styled from 'styled-components';
// import { useEffect } from 'react';

const SWrap = styled.div`
	height: 45px;
	background: white;
	z-index: 1;
	.nav-container {
		width: 100%;
		margin: auto;
		max-width: 1280px;
		img {
			&.logo {
				width: 36px;
				height: 36px;
			}
		}
	}
`;

const NavTop = () => {
	return (
		<SWrap className="d-flex bottom-border border-bottom position-fixed w-100">
			<div className="nav-container d-flex align-items-center justify-content-between">
				<img src={LOGO_PATH} alt="Asset logo" className="logo pointer" />
				<div className="d-inline-flex justify-content-end avatar">
					<NavUser />
				</div>
			</div>
		</SWrap>
	);
};

export default NavTop;
