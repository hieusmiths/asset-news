// @ts-nocheck
import React, { useRef } from 'react';
import { useSelector } from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import CommentList from './comment-list';
import CommentForm from './comment-form';
import CommentItem from './comment-item';
import { useGetListComment, useAddComment } from './hook';
/* eslint-disable */
const id = '5ea781fff3efec2524bdcb16';
const CommentBox = () => {
	let commentFormRef = useRef(null);
	const { fName, lName, avatar: currentUserAvatar, id: idUser } = useSelector(
		({ user }) => user
	);
	const currentUserName = fName + lName;
	const {
		comments,
		handleAddComment,
		isMore,
		fetchComments,
	} = useGetListComment(id, currentUserName, currentUserAvatar);
	const handleClearValue = () => {
		// @ts-ignore
		commentFormRef.clearForm();
	};
	const { handleSendComent } = useAddComment({
		id,
		onClear: handleClearValue,
		onAdd: handleAddComment,
		avatar: currentUserAvatar,
		userName: currentUserName,
		idUser,
	});

	return (
		/* eslint-disable-next-line */
		<React.Fragment>
			<CommentForm
				ref={(ref) => (commentFormRef = ref)}
				onKeyPress={handleSendComent}
			/>
			<div className="mb-3" />
			<CommentList
				data={comments}
				rootId={id}
				currentUserAvatar={currentUserAvatar}
				currentUserName={currentUserName}
				ChildComponent={CommentItem}
				idUser={idUser}
				end
			/>
			<Row>
				{isMore && (
					<Col>
						<span onClick={fetchComments} className="pointer">
							Xem thêm
						</span>
					</Col>
				)}
			</Row>
		</React.Fragment>
	);
};

export default CommentBox;
