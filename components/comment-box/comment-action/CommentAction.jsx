import React from 'react';
import { Popover, OverlayTrigger } from 'react-bootstrap';
import { ThreeDots } from '@icons';

const CommentAction = ({ onDelete, onEdit }) => {
	return (
		<OverlayTrigger
			rootClose
			trigger="click"
			placement="bottom"
			overlay={
				<Popover arrowProps={null} id="popover-nav-user">
					<Popover.Content>
						<div className="pointer">
							<div
								onClick={onEdit}
								onKeyDown={onEdit}
								role="button"
								tabIndex={0}
							>
								Chỉnh sửa
							</div>
							<div
								onClick={onDelete}
								onKeyDown={onDelete}
								role="button"
								tabIndex={0}
							>
								Xóa
							</div>
						</div>
					</Popover.Content>
				</Popover>
			}
		>
			<ThreeDots className="pointer" />
		</OverlayTrigger>
	);
};

export default CommentAction;
