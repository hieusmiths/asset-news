import React from 'react';
import styled from 'styled-components';

const SWrap = styled.div`
	span {
		&.action-item {
			padding: 3px 4px;
			color: var(--secondary-color);
			font-size: 12px;
			&.reply {
				right: 100%;
			}
			&.active {
			}
		}
	}
`;
/* eslint-disable-next-line */
const CommentFooter = ({ isLiked, onLikeClick, onReplyClick, count }) => {
	return (
		<SWrap className="d-flex justify-content-end">
			<div>
				<span className="action-item active pointer">
					<span>Thích</span>
				</span>
				<span
					className="action-item reply pointer"
					onClick={onReplyClick}
					onKeyDown={onReplyClick}
					role="button"
					tabIndex={0}
				>
					Bình luận &nbsp;
					<span>{count}</span>
				</span>
			</div>
		</SWrap>
	);
};

export default CommentFooter;
