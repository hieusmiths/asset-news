// @ts-nocheck
import React from 'react';

const CommentList = ({
	data = [],
	rootId,
	currentUserName,
	currentUserAvatar,
	end = false,
	ChildComponent,
	onReplyClick,
	idUser,
}) => {
	const genListComent = (list) => {
		if (list.lenght === 0) return null;
		const result = list.map((item) => {
			const { _id: commentId, title, user, children: listChildren } = item;
			const {
				avatar,
				userName,
				first_name: firstName,
				last_name: lastName,
				_id: createIdUser,
			} = user;
			const name = userName || `${firstName} ${lastName}`;
			const isMyCmt = createIdUser === idUser;
			return (
				<ChildComponent
					rootId={rootId}
					key={commentId}
					content={title}
					id={commentId}
					avatar={avatar}
					name={name}
					listChildren={listChildren}
					currentUserAvatar={currentUserAvatar}
					currentUserName={currentUserName}
					end={end}
					onReplyClick={onReplyClick}
					isMyCmt={isMyCmt}
					idUser={idUser}
				/>
			);
		});
		return result;
	};
	const comments = genListComent(data);
	return <div>{comments}</div>;
};

export default CommentList;
