/* eslint-disable */
import http from '@http';
import { COMMENT_ADD } from '@api/comment.api';

const useAddComment = ({
	id,
	parentId = null,
	type = 'post',
	onClear,
	onAdd,
	avatar,
	userName,
	idUser,
}) => {
	const handleSendComent = async (e) => {
		const isEnter = e.key === 'Enter';
		if (!isEnter) return;
		event.preventDefault();
		try {
			const { value } = e.target;
			const payload = {
				parent_id: parentId,
				rel_id: id,
				title: value,
			};
			const { status, data } = await http.postSth(COMMENT_ADD, payload);
			if (status) {
				onClear();
				const comment = {
					user: {
						avatar,
						userName,
						_id: idUser,
					},
					...data,
				};
				onAdd(comment);
			}
		} catch (error) {
			/* eslint-disable-next-line */
			console.log(error);
		}
	};

	return {
		handleSendComent,
	};
};

export default useAddComment;
