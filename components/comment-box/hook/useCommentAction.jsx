import React, { useState } from 'react';
import http from '@http';
import { COMMENT_REMOVE, COMMENT_UPDATE } from '@api/comment.api';
import { COMMENT_TYPES } from '@const';
import { isFunc } from '@utils/func.util';

const useCommentAction = () => {
	const [isDeleted, setIsDeleted] = useState(false);
	const [isEditing, setIsEditing] = useState(false);
	const [isUpdating, setIsUpdating] = useState(false);
	const handleOnDelete = async (id) => {
		try {
			const { status } = await http.putSth(COMMENT_REMOVE, { _id: id });
			if (status) setIsDeleted(true);
		} catch (error) {
			/* eslint-disable-next-line */
			console.log(error);
		}
	};
	const handleEdit = () => {
		setIsEditing(true);
	};
	const handleCancelEdit = async () => {
		setIsEditing(false);
	};
	const handleUpdate = async ({
		e: event,
		rootId,
		parentId,
		type = COMMENT_TYPES.POST,
		callback = null,
	}) => {
		const isEnter = event.key === 'Enter';
		if (!isEnter) return;
		event.preventDefault();
		const { value } = event.target || {};
		setIsUpdating(true);
		const payload = {
			rel_id: rootId,
			parent_id: parentId,
			title: value,
			type,
		};
		try {
			const { status } = http.postSth(COMMENT_UPDATE, payload);
			if (isFunc(callback)) {
			}
		} catch (error) {
			/* eslint-disable-next-line */
			console.log(error);
		}
	};
	return {
		isDeleted,
		handleOnDelete,
		isEditing,
		setIsEditing,
		handleEdit,
		handleCancelEdit,
		isUpdating,
		handleUpdate,
	};
};

export default useCommentAction;
