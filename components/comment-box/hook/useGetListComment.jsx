import { useEffect, useState } from 'react';
import { isArray } from 'lodash';
import http from '@http';
import { COMMENT_LIST } from '@api/comment.api';
import { isString } from '@utils/func.util';
import useGetOffsetDate from './useGetOffsetDate';
import useCommentAction from './useCommentAction';

const LIMIT = 5;

/* eslint-disable */
const commentFactory = ({ comment, avatar, name, idUser = null }) => {
	if (isString(comment)) {
		return {
			title: comment,
			avatar: avatar,
			name: name,
			idUser: null,
		};
	}
	return comment;
};

const fetchData = async ({ offsetDate = null, rootId }) => {
	const params = {
		rel_id: rootId,
		create_date: offsetDate,
		limit: LIMIT,
	};
	try {
		const { status, data } = await http.getSth(COMMENT_LIST, { params });
		if (isArray(data)) {
			return data;
		}
		// if (isArray(resp)) {
		// 	return resp;
		// }
		throw new Error('LIST_COMMENTS_IS_NOT_A_ARRAY');
	} catch (error) {
		throw new Error();
	}
};

const useGetListComment = (rootId, currentUserName, currentUserAvatar) => {
	const [comments, setComments] = useState([]);
	const [isMore, setIsMore] = useState(true);
	const [offsetDate, setOffsetDate] = useState(null);
	const { getOffsetDate } = useGetOffsetDate();
	const { handleOnDelete } = useCommentAction();
	const fetchComments = async () => {
		try {
			const resp = await fetchData({ rootId, offsetDate });
			const isMore = resp.length === LIMIT;
			setIsMore(isMore);
			setComments([...comments, ...resp]);
			const offsetTime = getOffsetDate(resp);
			setOffsetDate(offsetTime);
		} catch (error) {
			/* eslint-disable-next-line */
			console.log(error);
		}
	};
	const handleAddComment = (newComment) => {
		const payload = {
			comment: newComment,
			avatar: currentUserAvatar,
			name: currentUserName,
		};
		const commentObj = commentFactory(payload);
		const data = [{ ...commentObj }, ...comments];
		setComments(data);
	};
	useEffect(() => {
		fetchComments();
	}, []);
	return { comments, handleAddComment, isMore, fetchComments, handleOnDelete };
};

export default useGetListComment;
