const getLastCreateDate = (array, defaultValue) => {
	try {
		const lastItem = array[array.length - 1];
		const result = lastItem.create_date || defaultValue;
		return result;
	} catch (error) {
		/* eslint-disable-next-line */
		console.log(error);
		return defaultValue;
	}
};

const useGetOffsetDate = () => {
	const getOffsetDate = (list, defaultValue = null) => {
		return getLastCreateDate(list, defaultValue);
	};
	return { getOffsetDate };
};

export default useGetOffsetDate;
