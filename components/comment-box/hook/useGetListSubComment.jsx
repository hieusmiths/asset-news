import { useState } from 'react';
import { isArray } from 'lodash';
import http from '@http';
import { COMMENT_LIST } from '@api/comment.api';
import useGetOffsetDate from './useGetOffsetDate';
import CommentAction from '../comment-action';

const LIMIT = 100;

const fetchData = async ({ offsetDate, parentId, rootId }) => {
	const params = {
		create_date: offsetDate,
		parent_id: parentId,
		rel_id: rootId,
		limit: LIMIT,
	};
	try {
		const { data } = await http.getSth(COMMENT_LIST, { params });
		if (isArray(data)) {
			return data;
		}
		throw new Error('LIST_SUB_COMMENTS_IS_NOT_A_ARRAY');
	} catch (error) {
		throw new Error(error);
	}
};
const useGetListSubComment = ({ parentId, rootId, totalInit }) => {
	const [comments, setComments] = useState([]);
	const [offsetDate, setOffsetDate] = useState(null);
	const { getOffsetDate } = useGetOffsetDate();
	const total = comments.length;
	const handleAddComment = (newComment) => {
		const data = [...comments, { ...newComment }];
		setComments(data);
	};
	const handleFetchComments = async () => {
		if (totalInit === 0) return;
		try {
			const resp = await fetchData({ offsetDate, parentId, rootId });
			const newComments = [...comments, ...resp];
			const offsetTime = getOffsetDate(resp);
			setOffsetDate(offsetTime);
			setComments(newComments);
		} catch (error) {
			/* eslint-disable-next-line */
			console.log(error);
		}
	};
	return {
		comments,
		fetchComments: handleFetchComments,
		handleAddComment,
		total,
	};
};

export default useGetListSubComment;
