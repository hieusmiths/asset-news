// @ts-nocheck
import React, { useImperativeHandle, useRef, forwardRef } from 'react';
import { Form } from 'react-bootstrap';
import { PLEASE_ENTER_CMT } from '@placeholder';

const CommentForm = ({ onKeyPress, placeholder, defaultValue }, ref) => {
	let inputRef = useRef(null);
	const handleClearValue = () => {
		/* eslint-disable-next-line */
		inputRef.value = '';
	};
	const handleFocus = () => {
		try {
			inputRef.focus();
		} catch (error) {
			/* eslint-disable-next-line */
			console.log(error);
		}
	};
	useImperativeHandle(ref, () => {
		return {
			clearForm: handleClearValue,
			focusForm: handleFocus,
		};
	});
	return (
		<Form.Control
			/* eslint-disable-next-line */
			ref={(thisRef) => (inputRef = thisRef)}
			onKeyPress={onKeyPress}
			placeholder={placeholder}
			defaultValue={defaultValue}
		/>
	);
};

const CommentFormForwardRef = forwardRef(CommentForm);
CommentFormForwardRef.defaultProps = {
	placeholder: PLEASE_ENTER_CMT,
	defaultValue: '',
};
export default CommentFormForwardRef;
