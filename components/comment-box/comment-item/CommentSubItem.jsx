/* eslint-disable */
import React from 'react';
import { Image, Row, Col } from 'react-bootstrap';
import styled from 'styled-components';
import CommentFooter from '../comment-footer';
import CommentAction from '../comment-action';
import CommentForm from '../comment-form';
import { useCommentAction } from '../hook';

const SLeft = styled.div`
	margin-left: 7px;
`;

const SName = styled.span`
	font-size: 16px;
	color: var(--main-color);
	line-height: 24px;
	cursor: pointer;
`;

const CommentSubItem = ({
	content,
	avatar,
	name,
	onReplyClick,
	isMyCmt,
	id,
	rootId,
	parentId,
}) => {
	const {
		isDeleted,
		handleOnDelete,
		isEditing,
		handleUpdate,
		handleEdit,
	} = useCommentAction();
	if (isDeleted) return null;
	return (
		<Row>
			<Col xs={12}>
				<Row>
					<Col xs={12}>
						<div className="d-flex">
							<Image className="avatar-sm" src={avatar} roundedCircle />
							{isEditing && (
								<SLeft className="flex-grow-1">
									<CommentForm
										defaultValue={content}
										onKeyPress={(e) => handleUpdate({ e, parentId, rootId })}
									/>
								</SLeft>
							)}
							{!isEditing && (
								<SLeft>
									<div>
										<SName>{name}</SName>
										{isMyCmt && (
											<span>
												<CommentAction
													onEdit={handleEdit}
													onDelete={() => handleOnDelete(id)}
												/>
											</span>
										)}
									</div>
									{content}
								</SLeft>
							)}
						</div>
					</Col>
					<Col>
						<CommentFooter onReplyClick={onReplyClick} />
					</Col>
				</Row>
			</Col>
		</Row>
	);
};

export default CommentSubItem;
