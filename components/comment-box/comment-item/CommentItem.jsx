/* eslint-disable */
import React, { useState, useRef } from 'react';
import { Image, Row, Col } from 'react-bootstrap';
import styled from 'styled-components';
import { PLEASE_ENTER_RESP_CMT } from '@placeholder';
import CommentFooter from '../comment-footer';
import CommentList from '../comment-list';
import CommentForm from '../comment-form';
import CommentAction from '../comment-action';
import CommentSubItem from './CommentSubItem';
import { useAddComment, useGetListSubComment, useCommentAction } from '../hook';

const SLeft = styled.div`
	margin-left: 7px;
`;

const SName = styled.span`
	font-size: 16px;
	color: var(--main-color);
	line-height: 24px;
	cursor: pointer;
	margin-right: 8px;
`;

const CommentItem = ({
	content,
	id,
	avatar,
	name,
	listChildren,
	rootId,
	currentUserName,
	currentUserAvatar,
	end,
	isMyCmt,
	idUser,
}) => {
	let commentFormRef = useRef(null);
	const totalSubReplyInit = listChildren.length || 0;
	const [openReply, setOpenRely] = useState(false);
	const {
		comments,
		fetchComments,
		handleAddComment,
		total,
	} = useGetListSubComment({
		parentId: id,
		rootId,
		totalInit: totalSubReplyInit,
	});
	const { isDeleted, handleOnDelete, handleEdit } = useCommentAction();
	if (isDeleted) return null;
	const handleClearValue = () => {
		// @ts-ignore
		commentFormRef.clearForm();
	};
	const handleFocus = () => {
		// @ts-ignore
		commentFormRef.focusForm();
	};
	const { handleSendComent } = useAddComment({
		id: rootId,
		onClear: handleClearValue,
		onAdd: handleAddComment,
		avatar: currentUserAvatar,
		userName: currentUserName,
		parentId: id,
		idUser,
	});
	const handleClickReply = () => {
		handleFocus();
		if (openReply) return;
		setOpenRely(true);
		fetchComments();
	};
	const formClasses = end && openReply ? 'd-block' : 'd-none';
	return (
		<Row>
			<Col xs={12}>
				<Row>
					<Col xs={12}>
						<div className="d-flex">
							<Image className="avatar-sm" src={avatar} roundedCircle />
							<SLeft>
								<div>
									<SName>{name}</SName>
									{isMyCmt && (
										<span>
											<CommentAction
												onEdit={() => handleEdit(id)}
												onDelete={() => handleOnDelete(id)}
											/>
										</span>
									)}
								</div>
								{content}
							</SLeft>
						</div>
					</Col>
					<Col>
						<CommentFooter
							onReplyClick={handleClickReply}
							count={total || totalSubReplyInit}
						/>
					</Col>
				</Row>
			</Col>
			<Col xs={{ offset: 1 }}>
				<CommentList
					ChildComponent={(props) => (
						<CommentSubItem {...props} parentId={id} />
					)}
					rootId={rootId}
					data={comments}
					currentUserAvatar={currentUserAvatar}
					currentUserName={currentUserName}
					onReplyClick={handleClickReply}
					idUser={idUser}
				/>
				<div className={formClasses}>
					<CommentForm
						ref={(ref) => (commentFormRef = ref)}
						onKeyPress={handleSendComent}
						placeholder={PLEASE_ENTER_RESP_CMT}
					/>
				</div>
			</Col>
		</Row>
	);
};

export default CommentItem;
