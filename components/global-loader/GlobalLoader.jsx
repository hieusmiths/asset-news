// @ts-nocheck
import React from 'react';
import { useSelector } from 'react-redux';

const GlobalLoader = () => {
	const { loading } = useSelector(({ loaderReducer }) => loaderReducer);
	return (
		<div>
			Loading
			{loading}
		</div>
	);
};

export default GlobalLoader;
