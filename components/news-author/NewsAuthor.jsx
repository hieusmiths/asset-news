import React from 'react';
import styled from 'styled-components';
import { useFollow } from '@hook';
import { TEXT } from '@const';
import { formatToDayMonth } from '@utils/time.util';

const SWrap = styled.div`
	img {
		width: 32px;
		height: 32px;
	}
	.name {
		font-size: 14px;
	}
`;

const SFollowBtn = styled.button``;

const FollowBtn = ({ init }) => {
	const { isFollow, loading, handleOnClick } = useFollow({ init });
	return (
		<SFollowBtn
			className="border-0 rounded mt-3"
			disabled={loading}
			onClick={handleOnClick}
			type="button"
		>
			{isFollow ? TEXT.following : TEXT.follow}
		</SFollowBtn>
	);
};

const NewsAuthor = ({ avatar, name, time, isFollow }) => {
	const timeFormated = formatToDayMonth({ time });
	return (
		<SWrap>
			<div className="d-flex align-items-center">
				<div className="d-flex align-items-center pointer mr-2">
					<img src={avatar} alt="" className="rounded-circle mr-2" />
					<span className="font-weight-normal">{name}</span>
				</div>
				<small className="italic">{timeFormated}</small>
			</div>
			<div>
				<FollowBtn init={isFollow} />
			</div>
		</SWrap>
	);
};

export default NewsAuthor;
