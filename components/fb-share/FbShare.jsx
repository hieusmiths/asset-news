import React from 'react';
import { useFbShare } from '@hook';
import styled from 'styled-components';

const SBtn = styled.button`
	transition: all 0.2s;
	box-shadow: var(--box-shadow-depth-5);
`;

const FbShare = () => {
	const { openShareTab } = useFbShare({ isHref: true });
	return (
		/* eslint-disable-next-line */
		<React.Fragment>
			<SBtn type="button" onClick={openShareTab} className="border-0">
				Share fb
			</SBtn>
		</React.Fragment>
	);
};

export default FbShare;
