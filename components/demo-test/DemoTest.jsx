import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Hello = ({ userName }) => {
	const [count, setCount] = useState(0);
	return (
		<div className="greeting">
			Hello,
			{ userName }
			<span className="total">{ count }</span>
			!
			<div><button id="increase" type="button" onClick={() => setCount((prev) => prev + 1)}>Click Increase</button></div>
			<div><button id="decrease" type="button" onClick={() => setCount((prev) => prev - 1)}>Click Decrease</button></div>
		</div>
	);
};

Hello.propTypes = {
	userName: PropTypes.string
};

Hello.defaultProps = {
	userName: ''
};

export default Hello;
