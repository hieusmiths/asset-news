import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import DemoTest from '../DemoTest';

describe('<DemoTestComponent />', () => {
	it('Test snapshot', () => {
		const tree = mount(<DemoTest userName="Minh Hieu3" />);
		expect(toJson(tree)).toMatchSnapshot();
	});

	it('Has one class total', () => {
		const tree = mount(<DemoTest userName="Minh Hieu3" />);
		expect(tree.find('.total')).toHaveLength(1);
	});

	it('Test props change', () => {
		const tree = mount(<DemoTest userName="Minh Hieu" />);
		expect(tree.props().userName).toEqual('Minh Hieu');
		tree.setProps({ userName: 'Hieu' });
		expect(tree.props().userName).toEqual('Hieu');
	});

	it('Test click', () => {
		const tree = mount(<DemoTest userName="Minh Hieu" />);
		const buttonIncrease = tree.find('#increase');
		buttonIncrease.simulate('click');
		expect(tree.find('.total').text()).toEqual('1');
	});
});
