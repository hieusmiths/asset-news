/* eslint-disable */
import React from 'react';
import Link from 'next/link';

const Nav = () => (
	<nav>
		<ul>
			<li>
				<Link href="/">
					<a>Home</a>
				</Link>
			</li>
			<li>
				<Link href="/redux">
					<a>Redux</a>
				</Link>
			</li>
			<li>
				<Link href="/language">
					<a>Multi language</a>
				</Link>
			</li>
			<li>
				<Link href="/news">
					<a>News</a>
				</Link>
			</li>
		</ul>
	</nav>
);

export default Nav;
