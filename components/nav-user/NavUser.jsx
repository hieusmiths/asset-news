import React from 'react';
import { useSelector } from 'react-redux';
import { Popover, OverlayTrigger } from 'react-bootstrap';
import styled from 'styled-components';

const SRoot = styled.div`
	width: 32px;
	height: 32px;
	img {
		box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	}
`;

const SMenu = styled.div`
	width: 110px;
`;

const NavUser = () => {
	const { lName, avatar } = useSelector(({ user }) => user);
	return (
		<OverlayTrigger
			rootClose
			trigger="click"
			placement="bottom"
			overlay={
				<Popover arrowProps={null} id="popover-nav-user">
					<Popover.Content>
						<SMenu>
							<div className="pointer">Đăng xuất</div>
						</SMenu>
					</Popover.Content>
				</Popover>
			}
		>
			<SRoot className="d-inline-block pointer position-relative">
				<img src={avatar} alt="" className="rounded-circle avatar" />
			</SRoot>
		</OverlayTrigger>
	);
};

export default NavUser;
