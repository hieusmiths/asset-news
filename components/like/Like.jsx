import React from 'react';
import classNames from 'classnames';
import styled from 'styled-components';
import { useLike } from '@hook';
import { LikeOutline } from '@icons';

const SRoot = styled.button`
	&.active {
		color: var(--blue);
	}
	span {
		&.total {
			color: var(--gray);
		}
		&.icon {
			top: -3px;
		}
	}
`;

const Like = ({ init, type, total, payload }) => {
	const { isLiked, handleOnClick } = useLike({ init, type, payload });
	const className = classNames('pointer border-0 overflow-hidden', {
		active: isLiked,
	});
	return (
		<SRoot onClick={handleOnClick} className={className}>
			<span className="total">{total}</span>&nbsp;
			<span className="position-relative icon">
				<LikeOutline />
			</span>
		</SRoot>
	);
};

export default Like;
