/* eslint-disable */
import React, { useMemo } from 'react';
import { NEWS_ROUTING_DETAIL } from '@routing/news.routing';
import { ImageFallback } from '@component';
import { StyledCard } from './style';

const NewsItem = ({ title, thumbnail, slugUrl }) => {
	const hrefMemo = useMemo(() => NEWS_ROUTING_DETAIL({ slugUrl }), [slugUrl]);
	return (
		<a href={hrefMemo} target="_blank" rel="noreferrer" className="mb-3">
			<StyledCard>
				<div className="post">
					<div className="header_post">
						<ImageFallback src={thumbnail} />
					</div>
					<div className="body_post">
						<p>{title}</p>
					</div>
				</div>
			</StyledCard>
		</a>
	);
};

export default NewsItem;
