import styled from 'styled-components';

export const StyledCard = styled.div`
	width: 96%;
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	.post {
		width: 350px;
		height: 100%;
		display: flex;
		flex-direction: column;
		position: relative;
		.header_post {
			width: 100%;
			height: 230px;
			background: #ddd;
			top: 0;
			-webkit-transition: cubic-bezier(0.68, -0.55, 0.27, 01.55) 320ms;
			-moz-transition: cubic-bezier(0.68, -0.55, 0.27, 01.55) 320ms;
			-ms-transition: cubic-bezier(0.68, -0.55, 0.27, 01.55) 320ms;
			-o-transition: cubic-bezier(0.68, -0.55, 0.27, 01.55) 320ms;
			transition: cubic-bezier(0.68, -0.55, 0.27, 01.55) 320ms;
		}
		.body_post {
			width: 100%;
			height: 100%;
			margin: 24px 0px;
			background: #fff;
			text-align: justify;
			bottom: 0;
			display: flex;
			justify-content: center;
			-webkit-transition: cubic-bezier(0.68, -0.55, 0.27, 01.55) 320ms;
			-moz-transition: cubic-bezier(0.68, -0.55, 0.27, 01.55) 320ms;
			-ms-transition: cubic-bezier(0.68, -0.55, 0.27, 01.55) 320ms;
			-o-transition: cubic-bezier(0.68, -0.55, 0.27, 01.55) 320ms;
			transition: cubic-bezier(0.68, -0.55, 0.27, 01.55) 320ms;
			cursor: pointer;
			p {
				color: #212529;
				font-size: 15px;
				font-weight: initial;
			}
			.container_infos {
				width: 100%;
				display: flex;
				justify-content: space-between;
				position: absolute;
				bottom: 0;
				border-top: 1px solid rgba(0, 0, 0, 0.2);
				padding-top: 25px;
				.postedBy {
					display: flex;
					flex-direction: column;
					text-transform: uppercase;
					letter-spacing: 1px;
					font-size: 12px;
					span {
						font-size: 12px;
						text-transform: uppercase;
						opacity: 0.5;
						letter-spacing: 1px;
						font-weight: bold;
					}
				}
			}
		}
	}
	@media (max-width: 414px) {
		height: 50vh;
	}
`;
