import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { StyledRelativeNews } from './style';

const INIT_PLACEHOLDER =
	'https://miro.medium.com/max/1808/1*UIR5S5aNwSu7pQ_Wim7djQ.jpeg';
const FALLBACK_IMAGE =
	'https://i1.wp.com/blog.logrocket.com/wp-content/uploads/2020/06/react-router.png?w=730&ssl=1';

const useHandleFallbackImage = ({
	init = INIT_PLACEHOLDER,
	src,
	fallback = FALLBACK_IMAGE,
}) => {
	const loadedRef = useRef(false);
	const errorRef = useRef(false);
	const onError = (e) => {
		errorRef.current = true;
		e.target.onerror = null;
		e.target.src = fallback;
	};
	const onLoad = (e) => {
		loadedRef.current = true;
		e.target.src = src;
	};
	return { init, onError, onLoad };
};

const ImageFallback = ({ alt, src, className }) => {
	const { onError } = useHandleFallbackImage({ src });
	return (
		<StyledRelativeNews
			alt={alt}
			src={src}
			className={className}
			onError={onError}
		/>
	);
};

ImageFallback.defaultProps = {
	alt: '',
	className: '',
};

ImageFallback.propTypes = {
	src: PropTypes.string.isRequired,
	alt: PropTypes.string,
	className: PropTypes.string,
};

export default ImageFallback;
