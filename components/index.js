import NewsItem from './news-item';
import CommentBox from './comment-box';
import FbShare from './fb-share';
import ImageFallback from './image-fallback';
import NavTop from './nav-top';
import NavUser from './nav-user';
import NewsAuthor from './news-author';
import Like from './like';

export {
	NewsItem,
	CommentBox,
	FbShare,
	ImageFallback,
	NavTop,
	NavUser,
	NewsAuthor,
	Like,
};
