import React from 'react';
/* eslint-disable */
import { useSelector, useDispatch } from 'react-redux';
import PHONE from 'regex-snippet';

const ReduxDemo = () => {
	const { counter } = useSelector((state) => state);
	const dispatch = useDispatch();
	return (
		<React.Fragment key="demo">
			<div className="container">
				<div>
					<button type="button" onClick={() => dispatch({ type: 'INCREMENT' })}>CLICK ME</button>
					<h1>{counter}</h1>
				</div>
				<style jsx>
					{`
						div.container {
							width: 100vw;
							height: 100vh;
							display: flex;
							justify-content: center;
							align-items: center;
						}
						h1 {
							text-align: center;
						}
				`}
				</style>
			</div>
		</React.Fragment>
	);
};

export default ReduxDemo;
