const Home = () => null;

export const getServerSideProps = ({ res }) => {
	/* eslint-disable-next-line */
	res.writeHead(301, {
		Location: '/',
	});
	res.end();
};

export default Home;
