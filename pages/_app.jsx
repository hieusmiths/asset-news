import React, { memo } from 'react';
import App from 'next/app';
import Router from 'next/router';
import NProgress from 'nprogress';
import { compose } from 'recompose';
import { Provider } from 'react-redux';
import withReduxStore from '@conf-store/with-redux-store';
import { appWithTranslation } from '@i18n';
import Head from '@component/head';
import { VerifyLogin } from '@system';
import { ErrorPage } from '@layout';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@shared/sass/global.css';
import '@shared/sass/font-size.scss';
import '@shared/sass/themes/default.scss';
import '@shared/sass/reset.css';
import '@shared/sass/variables.css';

Router.events.on('routeChangeStart', () => {
	NProgress.start();
});
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const VerifyLoginMemo = memo(VerifyLogin);

const DefaultLayout = ({ children }) => (
	/* eslint-disable-next-line */
	<React.Fragment>{children}</React.Fragment>
);

class MyApp extends App {
	render() {
		const { Component, pageProps, store } = this.props;
		const { statusCode } = pageProps;
		const Layout = Component.Layout || DefaultLayout;
		if (statusCode === 404) {
			return <ErrorPage statusCode={statusCode} />;
		}
		return (
			<Provider store={store}>
				<Head />
				<VerifyLoginMemo />
				{/* eslint-disable-next-line react/jsx-props-no-spreading */}
				<Layout>
					<Component {...pageProps} />
				</Layout>
			</Provider>
		);
	}
}

export default compose(appWithTranslation, withReduxStore)(MyApp);
