import React from 'react';
import { useTranslation } from '@i18n';

export default () => {
	const [t, i18n] = useTranslation();
	return (
		<div className="container">
			<div>
				<button
					type="button"
					onClick={() =>
						i18n.changeLanguage(i18n.language === 'en' ? 'vi' : 'en')
					}
				>
					Revert
				</button>
				<h1>{t('name')}</h1>
			</div>
		</div>
	);
};
