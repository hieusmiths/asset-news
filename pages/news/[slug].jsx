/* @ts-nocheck */
/* eslint-disable */
import dynamic from 'next/dynamic';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Container, Row, Col, Button } from 'react-bootstrap';
import http from '@http';
import { useGetPostRelative } from '@hook';
import { NewsItem, CommentBox, Like } from '@component/index';
import { BaseLayout, ErrorPage } from '@layout/index.js';
import { POST_DETAIL_BY_SLUG } from '@api/new.api';
import { parseStringToHtml } from '@utils/string.uitl';
import { REACTION_TYPES } from '@const';

const FbShareNoSSR = dynamic(
	() => import('@component').then(({ FbShare }) => FbShare),
	{
		ssr: false,
	}
);

const Author = dynamic(
	() => import('@component').then(({ NewsAuthor }) => NewsAuthor),
	{
		ssr: false,
	}
);

const AuthorMemo = React.memo(Author);

// eslint-disable-next-line react/prop-types
const News = ({ data }) => {
	const idUser = useSelector(({ user }) => user.id);
	if (!data) return <ErrorPage statusCode={404} />;
	const {
		description: currentDesc,
		name: title,
		_id: idCurrent,
		create_uid: author,
		publish_date,
		isFollow,
		isLike,
		totalLike,
	} = data;
	const {
		display_name: authorName,
		_id: authorId,
		avatar: authorAvatar,
	} = author;
	const { list, fetchList } = useGetPostRelative({ id: idCurrent });
	const content = parseStringToHtml(data.content);
	useEffect(() => {
		fetchList();
	}, []);
	const likePayload = {
		_id: idCurrent,
		idUser,
	};
	return (
		<Container fluid="md">
			<Row className="justify-content-md-center">
				<Col xs={12} md={10}>
					<div>
						<h1 className="fw-400">{title}</h1>
					</div>
					<div className="mt-2 mb-5">
						<AuthorMemo
							time={publish_date}
							name={authorName}
							id={authorId}
							avatar={authorAvatar}
							isFollow={isFollow}
						/>
					</div>
					<div>
						<small>{currentDesc}</small>
					</div>
					{content}
				</Col>
				<Col xs={12} md={10}>
					<div className="d-flex justify-content-end align-items-center my-3">
						<div>
							<FbShareNoSSR />
							<span className="mx-3" />
							<Like
								init={isLike}
								type={REACTION_TYPES.POST}
								total={totalLike}
								payload={likePayload}
							/>
						</div>
					</div>
				</Col>
				<Col xs={12} md={10}>
					<CommentBox />
				</Col>
			</Row>
			<div className="rounded">
				<h4 className="mt-4 mb-4">Liên quan</h4>
				<Row>
					{list.map((item) => {
						const { name, description, image, _id: idItem, slugUrl } = item;
						return (
							<Col xs={12} md={6} lg={4} key={idItem}>
								<NewsItem
									title={name}
									description={description}
									thumbnail={image}
									slugUrl={slugUrl}
								/>
							</Col>
						);
					})}
				</Row>
			</div>
		</Container>
	);
};

export const getServerSideProps = async ({ params, req, res }) => {
	const { slug } = params;
	const { data } = await http.getSth(`${POST_DETAIL_BY_SLUG}?slug=${slug}`);
	return {
		props: { data },
	};
};

News.Layout = BaseLayout;

export default News;
