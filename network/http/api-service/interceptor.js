import axios from 'axios';
import { getCookies } from '@utils/cookie.util';
/* eslint-disable */
const API_SERVER = 'http://localhost:9002/api/v1';

const TOKEN = getCookies('token') || '';
const ROLE_TOKEN = getCookies('roleToken') || '';

const axiosInstance = axios.create({
	withCredentials: true,
	baseURL: API_SERVER,
	headers: {
		token: TOKEN,
		roleToken: ROLE_TOKEN,
	},
});

const successHandler = (response) => {
	const isStatus = response.data.status;
	const { status: statusCode } = response;
	if (isStatus) {
		return {
			status: true,
			data: response.data.data || null,
			statusCode,
		};
	}
	// Because current API not send http status so, ... Remove as soon.
	if (isStatus === false) {
		return {
			status: false,
			data: null,
		};
	}
	return {
		status: true,
		data: response.data || null,
		statusCode,
	};
};

const errorHandler = (error) => {
	// eslint-disable-next-line no-console
	const { status: statusCode } = error;
	return {
		status: false,
		error,
		statusCode,
		data: null,
	};
};

// axiosInstance.interceptors.request.use(
// 	(request) => {
// 		if (TOKEN != null) {
// 			request.headers.Authorization = `Bearer ${TOKEN}`;
// 		}
// 		return request;
// 	},
// 	(error) => {
// 		// eslint-disable-next-line no-console
// 		console.log(error);
// 		return false;
// 	}
// );

axiosInstance.interceptors.response.use(
	(response) => successHandler(response),
	(error) => errorHandler(error)
);

export default axiosInstance;
