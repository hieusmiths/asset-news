import http from '@http';
import { AUTH_AUTHENTICATE } from '@api/auth.api';

const verifyLogin = async () => {
	try {
		const { status, data } = await http.getSth(AUTH_AUTHENTICATE);
		return { status, data };
	} catch (error) {
		/* eslint-disable-next-line */
		console.log(error);
		return { status: false };
	}
};

const useVerifyLogin = () => {
	return { verifyLogin };
};

export default useVerifyLogin;
