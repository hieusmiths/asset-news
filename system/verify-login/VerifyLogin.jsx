import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setAuthLogin } from '@store/auth/auth.action';
import { setUserInfo } from '@store/user/user.action';
import useVerifyLogin from './useVerifyLogin';

const VerifyLogin = () => {
	const dispatch = useDispatch();
	const { verifyLogin } = useVerifyLogin();
	const handleVerifyLogin = async () => {
		const { status, data } = await verifyLogin();
		// Will clean and remove soon
		if (status) {
			const { isLogin, user: userInfo } = data;
			if (isLogin) {
				dispatch(setAuthLogin({ isLogin }));
				dispatch(setUserInfo(userInfo));
			}
		}
	};
	useEffect(() => {
		handleVerifyLogin();
	}, []);
	return null;
};

export default VerifyLogin;
