/* eslint-disable */
// require('babel-register')({
//   babelrc: false,
//   presets: [
//     [
//       'env',
//       {
//         targets: {
//           node: '8',
//         },
//       },
//     ],
//     'stage-3', // I use object-reset-spread 😀
//   ],
// })
const { setConfig } = require('next/config')
setConfig(require('./next.config'))

require('./server')