import devCofig from './dev.enviroment';
import prodConfig from './prod.enviroment';

/* eslint-disable */
const getEnvVariables = () => {
	const isDev = process.env.NODE_ENV === 'development';
	if (isDev) {
		return devCofig;
	}
	if (!isDev) {
		return prodConfig;
	}
};

const enviroment = getEnvVariables();

export default enviroment;
