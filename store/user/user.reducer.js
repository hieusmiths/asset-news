import { USER_SET_INFO } from './user.type';

const init = {
	avatar: '',
	fName: '',
	lName: '',
};

const userReducers = (state = { ...init }, { type, payload }) => {
	switch (type) {
		case USER_SET_INFO: {
			const { userInfo } = payload;
			return { ...state, ...userInfo };
		}

		default:
			return state;
	}
};

export default userReducers;
