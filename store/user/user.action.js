import { createAction } from '@utils/store.util';
import { USER_SET_INFO } from './user.type';

export const setUserInfo = (userInfo) => {
	return createAction(USER_SET_INFO, { userInfo });
};
