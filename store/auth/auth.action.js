import { createAction } from '@utils/store.util';
import { AUTH_SET_LOGIN } from './auth.type';

export const setAuthLogin = ({ isLogin }) => {
	return (dispatch) => {
		dispatch(createAction(AUTH_SET_LOGIN, { isLogin }));
	};
};
