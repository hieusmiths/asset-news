import { AUTH_SET_LOGIN } from './auth.type';

const init = {
	isLogin: false,
};

const authReducers = (state = { ...init }, { type, payload }) => {
	switch (type) {
		case AUTH_SET_LOGIN: {
			const { isLogin = false } = payload;
			return { ...state, isLogin };
		}
		default:
			return state;
	}
};

export default authReducers;
