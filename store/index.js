import { combineReducers } from 'redux';
import loaderReducer from './loader/loader.reducer';
import authReducers from './auth/auth.reducer';
import userReducers from './user/user.reducer';
import { counterReducer, timerReducer } from './demo/reducers';

// eslint-disable-next-line import/prefer-default-export
export const appReducer = combineReducers({
	auth: authReducers,
	user: userReducers,
	counter: counterReducer,
	timer: timerReducer,
	loaderReducer,
});
