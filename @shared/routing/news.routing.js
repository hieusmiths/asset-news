const PRE = '/news/';

export const NEWS_ROUTING_DETAIL = ({
	slugUrl = null,
	slug = null,
	squence = null,
}) => {
	if (slugUrl) return `${PRE}${slugUrl}`;
	return `${PRE}${slug}.${squence}`;
};
