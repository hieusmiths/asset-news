import { AiOutlineLike as LikeOutline } from 'react-icons/ai';
import { GoComment as Comment } from 'react-icons/go';
import { MdMoreHoriz as ThreeDots } from 'react-icons/md';

export { LikeOutline, Comment, ThreeDots };
