export const LOGO_PATH = 'https://www.asset.vn/assets/img/icon-asset.png';

export const COMMENT_TYPES = {
	POST: 'post',
};

export const REACTION_TYPES = {
	POST: 'post',
};

export const TEXT = {
	follow: 'Theo dõi',
	following: 'Đang theo dõi',
};
