const PRE = '/post';

export const POST_DETAIL_BY_SLUG = `${PRE}/detail`;
export const POST_RANDOM_RELATIVE = `${PRE}/get-post-relative`;
