const PRE = '/comment';
export const COMMENT_LIST = `${PRE}/list`;
export const COMMENT_ADD = `${PRE}/add`;
export const COMMENT_UPDATE = `${PRE}/update`;
export const COMMENT_REMOVE = `${PRE}/delete`;
