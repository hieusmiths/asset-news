/* eslint-disable no-param-reassign */
const path = require('path');

const withBundleAnalyzer = require('@next/bundle-analyzer')({
	enabled: process.env.ANALYZE === 'true',
});

/* eslint-disable */
module.exports = withBundleAnalyzer(() => ({
	webpack: (config, { dev }) => {
		if (dev) {
			config.module.rules.push({
				test: /\.(j|t)sx?$/,
				exclude: /node_modules/,
				loader: 'eslint-loader',
			});
		}
		config.resolve.alias['@component'] = path.join(__dirname, 'components');
		config.resolve.alias['@conf-store'] = path.join(__dirname, 'config/store');
		config.resolve.alias['@i18n'] = path.join(
			__dirname,
			'config/translations/config'
		);
		config.resolve.alias['@store'] = path.join(__dirname, 'store');
		config.resolve.alias['@env'] = path.join(__dirname, 'enviroments');
		config.resolve.alias['@http'] = path.join(__dirname, 'network/http');
		config.resolve.alias['@hook'] = path.join(__dirname, 'hook');
		config.resolve.alias['@api'] = path.join(__dirname, '@shared/api-client');
		config.resolve.alias['@utils'] = path.join(__dirname, 'utils');
		config.resolve.alias['@system'] = path.join(__dirname, 'system');
		config.resolve.alias['@icons'] = path.join(__dirname, '@shared/icons');
		config.resolve.alias['@routing'] = path.join(__dirname, '@shared/routing');
		config.resolve.alias['@layout'] = path.join(__dirname, '@layout');
		config.resolve.alias['@placeholder'] = path.join(
			__dirname,
			'@shared/placeholder'
		);
		config.resolve.alias['@const'] = path.join(__dirname, '@shared/consts');

		return config;
	},
	sassOptions: {
		includePaths: [
			path.join(__dirname, '@shared/sass'),
			path.join(__dirname, 'components/**/*'),
			path.join(__dirname, 'components/nav-top'),
		],
	},
}));
