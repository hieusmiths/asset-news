import { useEffect, useRef } from 'react';

const usePrevius = (value) => {
	const ref = useRef(null);
	useEffect(() => {
		ref.current = value;
	}, [value]);
	return ref.current;
};

export default usePrevius();
