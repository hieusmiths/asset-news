const THEMES = {
	LIGHT: 'light',
	DARK: 'dark',
};
const useChangeTheme = () => {
	const setTheme = (theme) => {
		try {
			document.documentElement.setAttribute('data-theme', theme);
		} catch (error) {
			/* eslint-disable-next-line */
			console.log(error);
		}
	};
	return { THEMES, setTheme };
};

export default useChangeTheme;
