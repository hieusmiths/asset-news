const useLogout = () => {
	const logout = () => {
		/* eslint-disable-next-line */
		console.log('LOGOUT');
	};
	return {
		logout,
	};
};

export default useLogout;
