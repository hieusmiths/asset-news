import { useMemo } from 'react';

const FB_SHARE_URL = 'https://www.facebook.com/dialog/share';
const useFbShare = ({ isHref = null, url = null }) => {
	const APP_ID = process.env.NEXT_PUBLIC_FB_APP_ID;
	const mergeHref = ({ href }) => {
		return `${FB_SHARE_URL}?app_id=${APP_ID}&display=popup&href=${href}`;
	};
	const getHref = ({ inputUrl, isCurrentHref }) => {
		if (isCurrentHref) {
			const currentHref = window.location.href;
			return mergeHref({ href: currentHref });
		}
		return mergeHref({ href: inputUrl });
	};
	const hrefMemo = useMemo(
		() => getHref({ inputUrl: url, isCurrentHref: isHref }),
		[isHref, url]
	);
	const openShareTab = () => {
		window.open(hrefMemo);
	};
	return {
		openShareTab,
	};
};

export default useFbShare;
