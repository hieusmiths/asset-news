import { useEffect, useRef } from 'react';

const useIsBrowser = () => {
	const ref = useRef(false);
	useEffect(() => {
		if (typeof window !== 'undefined') ref.current = true;
	}, []);
	return ref.current;
};

export default useIsBrowser;
