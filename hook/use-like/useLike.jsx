import { useState } from 'react';
import http from '@http';
import { REACTION_DEFAULT } from '@api/reaction.api';

const fetch = async (payload) => {
	const { status } = await http.postSth(REACTION_DEFAULT, payload);
	return status;
};

const useLike = ({ type, init, payload }) => {
	const [isLiked, setIsLiked] = useState(init);
	const [loading, setLoading] = useState(false);
	const handleOnClick = async () => {
		setLoading(true);
		setIsLiked((prev) => !prev);
		try {
			const status = await fetch({ ...payload, type });
			if (status) setIsLiked(true);
		} catch (error) {
			/* eslint-disable-next-line */
			console.log(error);
		} finally {
			setLoading(false);
		}
	};
	return { loading, handleOnClick, isLiked };
};

export default useLike;
