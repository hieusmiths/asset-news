import { useState } from 'react';
import http from '@http';

const fetch = async () => {
	const { status } = await http.postSth('user/follow/12');
	return status;
};

const useFollow = ({ init }) => {
	const [isFollow, setIsFollow] = useState(init);
	const [loading, setLoading] = useState(false);
	const handleOnClick = async () => {
		setLoading(true);
		try {
			const { status } = await fetch();
			if (status) {
				setIsFollow((pre) => !pre);
			}
		} catch (error) {
			/* eslint-disable-next-line */
			console.log(error);
		} finally {
			setLoading(false);
		}
	};
	return { isFollow, loading, handleOnClick };
};

export default useFollow;
