import { useState } from 'react';
import http from '@http';
import { POST_RANDOM_RELATIVE } from '@api/new.api';

const fetchData = async ({ id }) => {
	const params = {
		postID: id,
	};
	try {
		const { status, data } = await http.getSth(POST_RANDOM_RELATIVE, {
			params,
		});
		if (status) {
			return data;
		}
		throw new Error('CANNOT_GET_RELATIVE_POST');
	} catch (error) {
		throw new Error(error);
	}
};

const useGetPostRelative = ({ id }) => {
	const [list, setList] = useState([]);
	const fetchList = async () => {
		try {
			const resp = await fetchData({ id });
			setList(resp);
		} catch (error) {
			/* eslint-disable-next-line */
			console.log(error);
		}
	};
	return { list, fetchList };
};

export default useGetPostRelative;
