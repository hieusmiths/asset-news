import useLoader from './use-loader';
import useGetPostRelative from './use-get-post-relative';
import useFbShare from './use-fb-share';
import useLogout from './use-logout';
import useFollow from './use-follow';
import useLike from './use-like';

/* eslint-disable-next-line */
export {
	useLoader,
	useGetPostRelative,
	useFbShare,
	useLogout,
	useFollow,
	useLike,
};
