import React from 'react';
import { action } from '@storybook/addon-actions';
import { Button } from '@storybook/react/demo';
import Nav from '../components/nav';

export default { title: 'Button' };

export const withNav = () => <Nav />;

export const withText = () => <Button>Hello Button</Button>;

export const withEmoji = () => (
  <Button onClick={action('clicked')}>
    <span role="img" aria-label="so cool">
      😀 😎 👍 💯
    </span>
  </Button>
);
